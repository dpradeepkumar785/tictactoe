from logging import currentframe
import random
board = ["-","-","-",
         "-","-","-",
         "-","-","-"]
currentplayer ="X"
winner = None
gamerunning= True
#gma board
def printBoard(board):
    print(board[0]+ "|" + board[1]+"|" +board[2])
    print("-----------------")
    print(board[3]+ "|" + board[4]+"|" +board[5])
    print("-----------------")
    print(board[6]+ "|" + board[7]+"|" +board[8])
    print("-----------------")
#player input
def playerinput(board):
    inp = int(input("select a spot 1-9:"))
    if board[inp-1]== "-":
        board [inp-1]= currentplayer
    else:
        print("oops player is alredy at the spot")

#check for win or tie
def checkhorizontle(board):
    global winner
    if board[0]==board[1]==board[2] and board[0] != "-":
        winner =board[0]
        return True
    elif board[3]==board[4]==board[5] and board[3] != "-":
        winner =board[3]
        return True
    elif board[6]==board[7]==board[8] and board[6] != "-":
        winner =board[6]
        return True
def checkrow(board):
    if board[0]==board[3]==board[6] and board[0] != "-":
        winner =board[0]
        return True
    elif board[1]==board[4]==board[7] and board[1] != "-":
        winner =board[1]
        return True
    elif board[2]==board[5]==board[8] and board[2] != "-":
        winner =board[3]
        return True
def check_diagnoal(board):
    if board[0]==board[4]==board[8] and board[0] != "-":
        winner =board[0]
        return True
    elif board[2]==board[4]==board[6] and board[2] != "-":
        winner =board[2]
        return True
def checkifwin(board):
    global gamerunning
    if checkhorizontle(board):
        printBoard(board)
        print(f"the winner is {winner}")
        gamerunning = False
    elif checkrow(board):
        printBoard(board)
        print(f"the winner is {winner}")
        gamerunning = False
    elif check_diagnoal(board):
        printBoard(board)
        print(f"the winner is {winner}")
        gamerunning = False
def checkiftie(board):
    global gamerunning
    if "-" not in board:
        printBoard(board)
        print("its tie")
        gamerunning = False
def switchplayer(board):
    global currentplayer
    if currentplayer== "X":
        currentplayer = "O"
    else:
        currentplayer ="X"

while gamerunning :
    printBoard(board)
    playerinput(board)
    checkifwin(board)
    checkiftie(board)
    switchplayer(board)

